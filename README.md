# README #
How to choose the best store for your needs
While there are many online e-shopping options to choose from, there are some criteria you can use to narrow your search and find the right store in our list.

The store should meet your interests: Many online technology stores have dedicated their inventory and content to specific market segments. For example, some of the sites we have selected are more focused on robotics, while others may favor mechanical technology or model kits.
They should be competitively priced: Compare the overall competitiveness of the site in your niche. Some may have some great deals on an ocean of overpriced parts, others are more expensive, but offer a "one-stop" buying experience, so you can save a lot of time and effort.
They should keep what you need in stock: Is their core business based on items you intend to use regularly? The easier these components are to obtain, the less you have to slow down your creation process while you wait for missing parts.

https://www.khayie.com/